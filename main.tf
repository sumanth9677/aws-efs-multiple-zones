#main.tf
data "aws_vpc" "jenkins_security_group" {
  id = var.vpc_id
}

resource "aws_security_group" "efs_sg" {
  name        = "${var.environment}-efs-sg"
  description = "controls access to efs"

  vpc_id = var.vpc_id

  ingress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [data.aws_vpc.selected.cidr_block]
  }

  tags = merge(
    {
      "Name" = format("%s", "${var.environment}-efs-sg")
    },
    {
      "Environment" = format("%s", var.environment)
    },
    {
      "Project" = format("%s", var.project)
    },
    var.tags,
  )
}

resource "aws_efs_file_system" "efs" {
  encrypted        = var.encrypted
  performance_mode = var.performance_mode
  creation_token   = var.creation_token

  dynamic "lifecycle_policy" {
    for_each = var.transition_to_ia != null ? [var.transition_to_ia] : []

    content {
      transition_to_ia = lifecycle_policy.value
    }
  }

  tags = merge(
    {
      "Name" = format("%s", "${var.environment}-efs")
    },
    {
      "Environment" = format("%s", var.environment)
    },
    {
      "Project" = format("%s", var.project)
    },
    var.tags,
  )
}

resource "aws_efs_mount_target" "efs_mount_target" {
  count = length(var.subnet_ids)

  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = element(var.subnet_ids, count.index)
  security_groups = [aws_security_group.efs_sg.id]
}
